using UnityEngine;
using Michsky.MUIP;

public class PlayPauseButton : MonoBehaviour
{
    public ButtonManager myButton;
    public Sprite playIcon;
    public Sprite pauseIcon;
    public void ChangeIcon()
    {
        if (myButton.buttonIcon == playIcon)
        {
            myButton.SetIcon(pauseIcon);
            return;
        }
        else
        {
            myButton.SetIcon(playIcon);
            return;
        }
    }
}