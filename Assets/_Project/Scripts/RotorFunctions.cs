using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class RotorFunctions : MonoBehaviour
{
    public float speed;
    private bool isTimePaused = false;
    void Awake()
    {
        Time.timeScale = 0;
        isTimePaused = true;
    }

    void Update()
    {
        if (!isTimePaused)
        {
            RotorStart(gameObject, speed);
        }
    }
    
    void RotorStart(GameObject obj, float speed)
    {
        // Rotate the object around its local z-axis
        obj.transform.Rotate(speed * Time.deltaTime * Vector3.down, Space.Self);
    }
    
    public void StartStopRotor()
    {
        if (Time.timeScale == 0)
            {
            Time.timeScale = 1;
            isTimePaused = false;
            return;
            }
        if (Time.timeScale == 1)
            {
            Time.timeScale = 0;
            return;
            }
    }
}
