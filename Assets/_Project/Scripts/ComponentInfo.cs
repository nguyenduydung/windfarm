using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HighlightPlus;

public class ComponentInfo : MonoBehaviour
{
    public GameObject WindowRotor;
    public GameObject WindowNacelle;
    public GameObject WindowTower;
    public GameObject Rotor;
    public GameObject Nacelle;
    public GameObject Tower;
    public GameObject HighlightPlusManager;

    void Awake()
    {
        WindowRotor.SetActive(false);
    }

    public void CompInfo()
    {
        HighlightEffect lastSelected = HighlightEffect.lastSelected;
        if (WindowRotor.activeSelf || WindowNacelle.activeSelf || WindowTower.activeSelf)
        {
            WindowRotor.SetActive(false);
            WindowNacelle.SetActive(false);
            WindowTower.SetActive(false);
        }
        else if (lastSelected)
        {

            if (lastSelected.name == "Rotor")
            {
                WindowRotor.SetActive(false);
                WindowNacelle.SetActive(false);
                WindowTower.SetActive(false);

                WindowRotor.SetActive(true);
            }
            if (lastSelected.name == "Nacelle")
            {
                WindowRotor.SetActive(false);
                WindowNacelle.SetActive(false);
                WindowTower.SetActive(false);

                WindowNacelle.SetActive(true);
            }
            if (lastSelected.name == "Tower")
            {
                WindowRotor.SetActive(false);
                WindowNacelle.SetActive(false);
                WindowTower.SetActive(false);

                WindowTower.SetActive(true);
            }
        }
    }

    public void CloseWindow()
    {
        WindowRotor.SetActive(false);
        WindowNacelle.SetActive(false);
        WindowTower.SetActive(false);
    }
}